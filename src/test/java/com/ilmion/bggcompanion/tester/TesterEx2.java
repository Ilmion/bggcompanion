package com.ilmion.bggcompanion.tester;


import org.junit.jupiter.api.Test;

import com.ilmion.bggcompanion.entities.Boardgame;
import com.ilmion.bggcompanion.exceptions.BoardgamegeekException;
import com.ilmion.bggcompanion.filters.CollectionFilter;
import com.ilmion.bggcompanion.filters.FilterIncludeExclude;
import com.ilmion.bggcompanion.services.BggCollection;
import com.ilmion.bggcompanion.services.BggRequester;

public class TesterEx2 {

	@Test
    public void test() {
    
        try {
        	String user = "Ilmion"; // Ilmion, corkysru
        	
            CollectionFilter filter = new CollectionFilter();
            filter.setOwn(FilterIncludeExclude.INCLUDE);
            filter.setExcludeExpansion(true);
            filter.setAllowDoublon(false);
            
            long lStartTime = System.nanoTime();
            BggCollection collectionWithOutExpansion = BggRequester.getCollection(user, filter);
            long lCollecTime = System.nanoTime();
            System.out.println("Collec    : " + ((lCollecTime - lStartTime) / 1000000));
            //filter.setExcludeExpansion(true);
            //List<Boardgame> collectionWithOutExpansion = BggRequester.getCollection(user, filter);
            collectionWithOutExpansion.loadDetail(true);
            long lInitTime = System.nanoTime();
            System.out.println("Init    : " + ((lInitTime - lCollecTime) / 1000000));
            
            for(Boardgame boardgame : collectionWithOutExpansion.getCollection()) {
                System.out.println(String.format("%s [%s] %d", boardgame.getName(), boardgame.getId(), boardgame.getMechanics().size()));
            }

            System.out.println("Collec nb game   : " + collectionWithOutExpansion.getCollection().size());
            
            long lEndTime = System.nanoTime();
            System.out.println("");
            System.out.println("Start to Finish : " + ((lEndTime - lStartTime) / 1000000));
            System.out.println("");
            System.out.println("Collec    : " + ((lCollecTime - lStartTime) / 1000000));
            System.out.println("Init      : " + ((lInitTime - lCollecTime) / 1000000));
             
        }
        catch(BoardgamegeekException e){
            System.out.println("Message from boardgamegeek : " + e.getMessage());
        }
    }

}
