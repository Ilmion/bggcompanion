package com.ilmion.bggcompanion.filters;

public class CollectionFilter {
    private FilterIncludeExclude own;
    private boolean expansion;
    private boolean allowDoublon = true;
    private FilterIncludeExclude preordered;

    private FilterIncludeExclude wishlist;

	public FilterIncludeExclude getOwn() {
        return own;
    }

    public void setOwn(FilterIncludeExclude own) {
        this.own = own;
    }
    
    public boolean getExcludeExpansion() {
        return expansion;
    }

    public void setExcludeExpansion(boolean expansion) {
        this.expansion = expansion;
    }
    
    public boolean isAllowDoublon() {
		return allowDoublon;
	}

	public void setAllowDoublon(boolean allowDoublon) {
		this.allowDoublon = allowDoublon;
	}
	
	public FilterIncludeExclude getPreordered() {
        return preordered;
    }

    public void setPreordered(FilterIncludeExclude preordered) {
        this.preordered = preordered;
    }

    public FilterIncludeExclude getWishlist() {
        return wishlist;
    }

    public void setWishlist(FilterIncludeExclude wishlist) {
        this.wishlist = wishlist;
    }

}
