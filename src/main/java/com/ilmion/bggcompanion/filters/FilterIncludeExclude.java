package com.ilmion.bggcompanion.filters;

public enum FilterIncludeExclude {
    UNSPECIFIED, INCLUDE, EXCLUDE
}
