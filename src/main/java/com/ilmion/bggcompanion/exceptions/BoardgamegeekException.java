package com.ilmion.bggcompanion.exceptions;

public class BoardgamegeekException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BoardgamegeekException(String message) {
        super(message);
    }
}
