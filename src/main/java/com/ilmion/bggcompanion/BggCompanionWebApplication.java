package com.ilmion.bggcompanion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

import com.ilmion.bggcompanion.listener.DatabaseSetup;

@SpringBootApplication
public class BggCompanionWebApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(BggCompanionWebApplication.class, args);
    }
    
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(BggCompanionWebApplication.class);
    }
    
    @Bean
    public DatabaseSetup datbaseSetup(){
        return new DatabaseSetup();
    }
}
