package com.ilmion.bggcompanion.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Play {
    private String id;
    private Date date;
    private String location;
    private Integer quantity;
    private boolean incomplete;
    private boolean nowinstats;
    private String comments;
    private String bggUser;
    
    public String getComments() {
        return comments;
    }
    public void setComments(String comments) {
        this.comments = comments;
    }
    private List<PlayPlayer> playPlayers = new ArrayList<PlayPlayer>();
    
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public Date getDate() {
        return date;
    }
    public void setDate(Date date) {
        this.date = date;
    }
    
    public String getLocation() {
        return location;
    }
    public void setLocation(String location) {
        this.location = location;
    }
    
    public Integer getQuantity() {
        return quantity;
    }
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
    public boolean isIncomplete() {
        return incomplete;
    }
    public void setIncomplete(boolean incomplete) {
        this.incomplete = incomplete;
    }
    public boolean isNowinstats() {
        return nowinstats;
    }
    public void setNowinstats(boolean nowinstats) {
        this.nowinstats = nowinstats;
    }
    public List<PlayPlayer> getPlayPlayers() {
        return playPlayers;
    }
    public void setPlayPlayers(List<PlayPlayer> playPlayers) {
        this.playPlayers = playPlayers;
    }
    public String getBggUser() {
        return bggUser;
    }
    public void setBggUser(String bggUser) {
        this.bggUser = bggUser;
    }
}
