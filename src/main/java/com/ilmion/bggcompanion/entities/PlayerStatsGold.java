package com.ilmion.bggcompanion.entities;

import java.util.Map;

public class PlayerStatsGold {
    private Player player;
    private Integer NbPlay;
    private Integer nbWin;
    
    // Nombre de fois que l'on a joué à chacun des boardgames que l'on a joué.
    private Map<Boardgame, Integer> boardgamePlayed;

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Integer getNbPlay() {
        return NbPlay;
    }

    public void setNbPlay(Integer nbPlay) {
        NbPlay = nbPlay;
    }

    public Integer getNbWin() {
        return nbWin;
    }

    public void setNbWin(Integer nbWin) {
        this.nbWin = nbWin;
    }

    public Map<Boardgame, Integer> getBoardgamePlayed() {
        return boardgamePlayed;
    }

    public void setBoardgamePlayed(Map<Boardgame, Integer> boardgamePlayed) {
        this.boardgamePlayed = boardgamePlayed;
    }
}
