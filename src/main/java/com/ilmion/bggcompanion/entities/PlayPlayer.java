package com.ilmion.bggcompanion.entities;

public class PlayPlayer {
    private Play play;
    private Player player;
    
    private Integer score;
    private boolean win;
    private String color;
    private Integer startposition;
    
    public Play getPlay() {
        return play;
    }
    public void setPlay(Play play) {
        this.play = play;
    }
    public Player getPlayer() {
        return player;
    }
    public void setPlayer(Player player) {
        this.player = player;
    }
    public Integer getScore() {
        return score;
    }
    public void setScore(Integer score) {
        this.score = score;
    }
    public boolean isWin() {
        return win;
    }
    public void setWin(boolean win) {
        this.win = win;
    }
    public String getColor() {
        return color;
    }
    public void setColor(String color) {
        this.color = color;
    }
    public Integer getStartposition() {
        return startposition;
    }
    public void setStartposition(Integer startposition) {
        this.startposition = startposition;
    }
}
