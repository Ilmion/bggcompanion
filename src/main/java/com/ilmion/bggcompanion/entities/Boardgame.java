package com.ilmion.bggcompanion.entities;

import java.util.ArrayList;
import java.util.List;

public class Boardgame {
    private String id;
    private String name;
    private List<Mechanic> mechanics;
    private List<Category> categories;
    private List<Boardgame> expansions;
    private boolean complete;
    private boolean detailled;
    
    private boolean own;
    private boolean preordered;
    private boolean wishlist;
    
    public boolean isDetailled() {
		return detailled;
	}

	public void setDetailled(boolean detailled) {
		this.detailled = detailled;
	}

	public Boardgame() {
        mechanics = new ArrayList<Mechanic>();
        categories = new ArrayList<Category>();
        expansions = new ArrayList<Boardgame>();
        detailled = false;
    }
    
    public boolean isComplete() {
        return complete;
    }
    public void setComplete(boolean complete) {
        this.complete = complete;
    }
    public List<Mechanic> getMechanics() {
        return mechanics;
    }
    public void setMechanics(List<Mechanic> mechanics) {
        this.mechanics = mechanics;
    }
    public List<Category> getCategories() {
        return categories;
    }
    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }
    public List<Boardgame> getExpansions() {
        return expansions;
    }
    public void setExpansions(List<Boardgame> expansions) {
        this.expansions = expansions;
    }
    
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    
    public boolean hasMechanics(String id) {
        for(Mechanic mechanic : mechanics) {
            if(mechanic.getId().compareTo(id) == 0){
                return true;
            }
        }
        return false;
    }
    
    public boolean hasCategory(String id) {
        for(Category category : categories) {
            if(category.getId().compareTo(id) == 0){
                return true;
            }
        }
        return false;
    }

    public void init(Boardgame boardgame) {
        this.mechanics = boardgame.getMechanics();
        this.categories = boardgame.getCategories();
        this.expansions = boardgame.getExpansions();
        detailled = true;
    }

	public boolean isOwned() {
		return own;
	}
	public void setOwned(boolean own) {
        this.own = own;
    }
	
	public boolean isPreordered() {
        return preordered;
    }

    public void setPreordered(boolean preordered) {
        this.preordered = preordered;
    }

    public boolean isWishlist() {
        return wishlist;
    }

    public void setWishlist(boolean wishlist) {
        this.wishlist = wishlist;
    }
	
	// TODO : <link type="boardgamefamily" id="23532" value="Promotional Cards"/>
}
