package com.ilmion.bggcompanion.entities;

public class BoardgameStats {
    private Boardgame boardgame;
    private Integer NbPlay;
    private Integer NbPlayCompleted;
    //private Integer nbWin;
    
    public Boardgame getBoardgame() {
        return boardgame;
    }
    public void setBoardgame(Boardgame boardgame) {
        this.boardgame = boardgame;
    }
    public Integer getNbPlay() {
        return NbPlay;
    }
    public void setNbPlay(Integer nbPlay) {
        NbPlay = nbPlay;
    }
    public Integer getNbPlayCompleted() {
        return NbPlayCompleted;
    }
    public void setNbPlayCompleted(Integer nbPlayCompleted) {
        NbPlayCompleted = nbPlayCompleted;
    }
}
