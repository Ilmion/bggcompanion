package com.ilmion.bggcompanion.entities;

public class PlayerStats {
    private Player player;
    private Integer nbPlay;
    private Integer nbDayPlayed;
    private Integer nbWin;
    private Integer nbPlayCompleted;
    private Integer nbWinCompleted;

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Integer getNbDayPlayed() {
        return nbDayPlayed;
    }

    public void setNbDayPlayed(Integer nbDayPlayed) {
        this.nbDayPlayed = nbDayPlayed;
    }

    public Integer getNbPlayCompleted() {
        return nbPlayCompleted;
    }

    public void setNbPlayCompleted(Integer nbPlayCompleted) {
        this.nbPlayCompleted = nbPlayCompleted;
    }

    public Integer getNbWinCompleted() {
        return nbWinCompleted;
    }

    public void setNbWinCompleted(Integer nbWinCompleted) {
        this.nbWinCompleted = nbWinCompleted;
    }

    public Integer getNbPlay() {
        return nbPlay;
    }

    public void setNbPlay(Integer nbPlay) {
        this.nbPlay = nbPlay;
    }

    public Integer getNbWin() {
        return nbWin;
    }

    public void setNbWin(Integer nbWin) {
        this.nbWin = nbWin;
    }
}
