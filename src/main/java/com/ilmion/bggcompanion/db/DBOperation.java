package com.ilmion.bggcompanion.db;

import java.sql.Connection;
import java.sql.SQLException;

public class DBOperation {
    public static void CreateTables(Connection con) throws SQLException {
        con.createStatement().executeUpdate("CREATE TABLE plays (play_id INTEGER, bgg_id INTEGER, play_date DATE, location VARCHAR(256), incomplete BOOLEAN, nowinstats BOOLEAN, comments VARCHAR(1024), bgguser VARCHAR(30))");
        con.createStatement().executeUpdate("CREATE TABLE plays_players (play_id INTEGER, name VARCHAR(256), win BOOLEAN , score DECIMAL(12,6))");
        con.createStatement().executeUpdate("CREATE TABLE games (bgg_id INTEGER, name VARCHAR(256), minplayers SMALLINT, maxplayers SMALLINT, rating DECIMAL(4,2), rank SMALLINT, own BOOLEAN, weight DECIMAL(3,2))");
        con.setAutoCommit(false);
    }
    
    public static void DonnerTest(Connection con) throws SQLException {
        con.createStatement().executeUpdate("INSERT INTO plays VALUES (1,1,'2017-06-01','LQ', FALSE, 'ilmion')");
        con.createStatement().executeUpdate("INSERT INTO games VALUES (1,'Avalon',2,4,8,500,FALSE,2)");
        
        con.createStatement().executeUpdate("INSERT INTO plays_players VALUES (1,'Bruno',TRUE,10)");
        con.createStatement().executeUpdate("INSERT INTO plays_players VALUES (1,'Gustavo',FALSE,5)");
        con.createStatement().executeUpdate("INSERT INTO plays_players VALUES (1,'Denis',FALSE,5)");
        
        con.createStatement().executeUpdate("INSERT INTO plays VALUES (2,1,'2017-06-01','LQ', FALSE, 'ilmion')");
        
        con.createStatement().executeUpdate("INSERT INTO plays_players VALUES (2,'Bruno',FALSE,10)");
        con.createStatement().executeUpdate("INSERT INTO plays_players VALUES (2,'Gustavo',TRUE,15)");
        con.createStatement().executeUpdate("INSERT INTO plays_players VALUES (2,'Denis',FALSE,5)");
        
        con.createStatement().executeUpdate("INSERT INTO plays VALUES (3,2,'2017-06-01','Guillaume', FALSE, 'ilmion')");
        con.createStatement().executeUpdate("INSERT INTO games VALUES (2,'7 Wonders Duel',2,2,8,200,FALSE,2)");
        
        con.createStatement().executeUpdate("INSERT INTO plays_players VALUES (3,'Bruno',FALSE,55)");
        con.createStatement().executeUpdate("INSERT INTO plays_players VALUES (3,'Guillaume',TRUE,62)");
        
        con.createStatement().executeUpdate("INSERT INTO plays VALUES (4,1,'2017-05-31','LQ', FALSE, 'ilmion')");
        
        con.createStatement().executeUpdate("INSERT INTO plays_players VALUES (4,'Bruno',FALSE,10)");
        con.createStatement().executeUpdate("INSERT INTO plays_players VALUES (4,'Gustavo',TRUE,15)");
        con.createStatement().executeUpdate("INSERT INTO plays_players VALUES (4,'Alain',FALSE,6)");
        
        con.createStatement().executeUpdate("INSERT INTO plays VALUES (5,3,'2017-06-02','LQ', FALSE, 'ilmion')");
        con.createStatement().executeUpdate("INSERT INTO games VALUES (3,'Colt Express',2,6,8,300,FALSE,1.2)");
        
        con.createStatement().executeUpdate("INSERT INTO plays_players VALUES (5,'Bruno',FALSE,7)");
        con.createStatement().executeUpdate("INSERT INTO plays_players VALUES (5,'Gustavo',FALSE,12)");
        con.createStatement().executeUpdate("INSERT INTO plays_players VALUES (5,'Denis',TRUE,16)");
        con.createStatement().executeUpdate("INSERT INTO plays_players VALUES (5,'Sébastien',FALSE,8)");
        con.commit();
    }
}
