package com.ilmion.bggcompanion.listener;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.ilmion.bggcompanion.db.DBOperation;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;

public class DatabaseSetup implements ServletContextListener {
    
    Connection con;
    //Server server;

    @Override
    public void contextDestroyed(ServletContextEvent arg0) {
        
    }

    @Override
    public void contextInitialized(ServletContextEvent arg0) {
        try {
        	Class.forName("org.hsqldb.jdbc.JDBCDriver");
            con = DriverManager.getConnection("jdbc:hsqldb:mem:mymemdb", "SA", ""); //
            
            DBOperation.CreateTables(con);
              
            //con.createStatement().executeUpdate("CREATE TABLE last_sync (date_time DATE)");
            ServletContext context = arg0.getServletContext();
            context.setAttribute("con", con);
        } catch (SQLException e) {
            e.printStackTrace(System.out);
        } catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}        
    }
}

/*
CREATE TABLE plays (play_id, bgg_id, play_date, quantity, length, location);
CREATE TABLE plays_players (play_id, name, win, score);
CREATE TABLE games (bgg_id, name, minplayers, maxplayers, rating, rank, own, weight);
CREATE TABLE last_sync (date_time);
*/
