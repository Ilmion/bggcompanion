package com.ilmion.bggcompanion.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ilmion.bggcompanion.entities.Boardgame;
import com.ilmion.bggcompanion.exceptions.BoardgamegeekException;

public class BggCollection {
	private Map<String, Boardgame> collectionData;
	private List<Boardgame> collectionTree;
	
	public BggCollection() {
		collectionData = new HashMap<String, Boardgame>();
		collectionTree = new ArrayList<Boardgame>();
	}
	
	public boolean ownBoardgame(String id){
		boolean result = false;
		if(collectionData.containsKey(id)) {
			result = collectionData.get(id).isOwned();
		}
		return result;
	}
	
	public void addBoardgame(Boardgame boardgame) {
		if(!collectionData.containsKey(boardgame.getId())) {
			collectionData.put(boardgame.getId(), boardgame);
			collectionTree.add(boardgame);
		}
		else {
			Boardgame tempBoardgame = collectionData.get(boardgame.getId());
			collectionTree.add(tempBoardgame);
		}
	}
	
	public List<Boardgame> getCollection(){
		return collectionTree;
	}
	
	 public void loadDetail(boolean initExpansion) throws BoardgamegeekException {
	        List<String> ids = new ArrayList<String>();
	        
	        for(Boardgame boardgame : collectionTree) {
	            ids.add(boardgame.getId());
	        }	        
	        while(ids.size() > 0){
	        	Map<String, Boardgame> newBoardgames = BggRequester.getBoardgames(ids);
	        	if(newBoardgames.size() > ids.size()) {
	        		throw new BoardgamegeekException("The number of thing return by the boardgamegeek api is not the same that was requested.");
	        	}
	        	ids = new ArrayList<String>();
	        	for(String key : newBoardgames.keySet()) {
	        		collectionData.get(key).init(newBoardgames.get(key));
	        		if(initExpansion){
	        			for(Boardgame expansion : collectionData.get(key).getExpansions()) {
	        				if(!collectionData.containsKey(expansion.getId())) {
	        					collectionData.put(expansion.getId(), expansion);
	        					ids.add(expansion.getId());
	        				}
	        			}
		        	}
	        	}	        	
	        }
	    }

    public Boardgame get(String id) {
        // TODO Auto-generated method stub
        return collectionData.get(id);
    }
}
