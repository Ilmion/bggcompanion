package com.ilmion.bggcompanion.services;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.ilmion.bggcompanion.entities.Boardgame;
import com.ilmion.bggcompanion.entities.Category;
import com.ilmion.bggcompanion.entities.Mechanic;
import com.ilmion.bggcompanion.exceptions.BoardgamegeekException;
import com.ilmion.bggcompanion.filters.CollectionFilter;
import com.ilmion.bggcompanion.http.HttpUtil;
import com.ilmion.bggcompanion.http.URLFactory;

public class BggRequester {
    private static int idChunkSize = 20;
          
    public static List<Mechanic> getMechanicsList(){
        List<Mechanic> mechanics = new ArrayList<Mechanic>();
        
        try {        	
            String mechanicPage = HttpUtil.getHttpContent(URLFactory.getMechanicUrl());
                             
	        Document doc = Jsoup.parse(mechanicPage);
	        Elements ahrefMechanics = doc.select("a[href^='/boardgamemechanic/']");
	        
	        for(Element element : ahrefMechanics) {
	            String[] attribPart = element.attr("href").split("/");
	            Mechanic mechanic = new Mechanic();
	            mechanic.setName(element.html());
	            mechanic.setId(attribPart[2]);
	            mechanics.add(mechanic);
	        }
        }
        catch(Exception e){
            e.printStackTrace();
        }

        return mechanics;       
    }
    
    public static List<Category> getCategoryList(){
        List<Category> categories = new ArrayList<Category>();
        
        
        try {
	        String categoryPage = HttpUtil.getHttpContent(URLFactory.getCategoryUrl());
                     
	        Document doc = Jsoup.parse(categoryPage);
	        Elements ahrefMechanics = doc.select("a[href^='/boardgamecategory/']");
	
	        for(Element element : ahrefMechanics) {
	            String[] attribPart = element.attr("href").split("/");
	            Category category = new Category();
	            category.setName(element.html());
	            category.setId(attribPart[2]);
	            categories.add(category);
	        }
        }
        catch(Exception e){
            e.printStackTrace();
        }

        return categories;       
    }
 
    public static String getBggXML(String URL) throws BoardgamegeekException {

        String bggResponse = HttpUtil.getHttpContent(URL);
        
        if(bggResponse == null){
            throw new BoardgamegeekException("An error occured while contacting Boardgamegeek. Please try again.");
        }
        
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            org.w3c.dom.Document responseXML = builder.parse(new InputSource(new ByteArrayInputStream(bggResponse.getBytes("utf-8"))));
                        
            XPath xPath = XPathFactory.newInstance().newXPath();
            String message = xPath.evaluate("//message", responseXML.getDocumentElement());
            if(message != null && message.length() > 0) {
                throw new BoardgamegeekException(message);
            }
            
        } catch (ParserConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SAXException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (XPathExpressionException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        
        return bggResponse;
    }
    
    public static BggCollection getCollection(String user) throws BoardgamegeekException {
        return getCollection(user, null);
    }
    
    public static BggCollection getCollection(String user, CollectionFilter collectionFilters) throws BoardgamegeekException {
    	BggCollection collection = new BggCollection();
        Set<String> idSet = new HashSet<String>();
        
        boolean allowDoublon = true;
        if(collectionFilters != null) {
            allowDoublon = collectionFilters.isAllowDoublon();
        }       
        
        String RealURL = URLFactory.getCollectionUrl(user, collectionFilters);
        System.out.println(RealURL);
        String collectionXML = getBggXML(RealURL);
        
        try {
            
            Boardgame newBoardgame = null;
            
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            InputStream in = new ByteArrayInputStream(collectionXML.getBytes("utf-8"));
            XMLStreamReader streamReader = inputFactory.createXMLStreamReader(in);
            streamReader.nextTag(); // Advance to "items" element
            while (streamReader.hasNext()) {
                if (streamReader.isStartElement()) {
                    switch (streamReader.getLocalName()) {
                        case "item": {
                            newBoardgame = new Boardgame();
                            newBoardgame.setId(streamReader.getAttributeValue(null, "objectid"));
                            break;
                        }
                        case "name": {
                            newBoardgame.setName(streamReader.getElementText().replaceAll("�", "-"));
                            break;
                        }
                        case "status": {
                            String strOwn = streamReader.getAttributeValue(null, "own");
                            String strWishlist = streamReader.getAttributeValue(null, "wishlist");
                            String strPreordered = streamReader.getAttributeValue(null, "preordered");
                            newBoardgame.setOwned(strOwn != null && strOwn.equals("1"));
                            newBoardgame.setWishlist(strWishlist != null && strWishlist.equals("1"));
                            newBoardgame.setPreordered(strPreordered != null && strPreordered.equals("1"));
                        }
                    }
                }
                if (streamReader.isEndElement()) {
                    if (streamReader.getLocalName() == "item") {
                    	if(allowDoublon || !idSet.contains(newBoardgame.getId())){
                    		idSet.add(newBoardgame.getId());
                    		collection.addBoardgame(newBoardgame);
                    	}
                    }
                }
                streamReader.next();
            }
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (XMLStreamException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
                     
        return collection;
    }
    
    public static Map<String, Boardgame> getBoardgames(List<String> ids) throws BoardgamegeekException {
        
    	System.out.println("Number of id : " + ids.size());
    	
    	Map<String, Boardgame> boardgames = new HashMap<String, Boardgame>();
        
    	try {
	    	int indexList = 0;
	    	do {
	    		StringBuilder param = new StringBuilder();
	    		for(int index = 0; index < idChunkSize && indexList < ids.size(); index++) {
	    			if(index!=0){
	                    param.append(",");
	                }
	    			param.append(ids.get(indexList));
	    			++indexList;
	    		}
	    		
	    		String realUrl = URLFactory.getThingUrl(param.toString());
	    		System.out.println(realUrl);
	    		
	    		String boardgamesXML = null;
	    		boardgamesXML = getBggXML(realUrl);           
	            
	            Boardgame newBoardgame = null;
	            List<Mechanic> newMechanics = null;
	            List<Category> newCategories = null;
	            List<Boardgame> newExpansions = null;
	            
	            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
	            InputStream in = new ByteArrayInputStream(boardgamesXML.getBytes("UTF-8"));
	            XMLStreamReader streamReader = inputFactory.createXMLStreamReader(in);
	            streamReader.nextTag(); // Advance to "items" element
	            while (streamReader.hasNext()) {
	                if (streamReader.isStartElement()) {
	                    switch (streamReader.getLocalName()) {
	                        case "item": {
	                            newBoardgame = new Boardgame();
	                            newBoardgame.setId(streamReader.getAttributeValue(null, "id"));
	                            newMechanics = new ArrayList<Mechanic>();
	                            newCategories = new ArrayList<Category>();
	                            newExpansions = new ArrayList<Boardgame>();
	                            newBoardgame.setMechanics(newMechanics);
	                            newBoardgame.setCategories(newCategories);
	                            newBoardgame.setExpansions(newExpansions);
	                            break;
	                        }
	                        case "name": {
	                            if(streamReader.getAttributeValue(null, "type").compareTo("primary") == 0){
	                                newBoardgame.setName(streamReader.getElementText().replaceAll("�", "-"));
	                            }
	                            break;
	                        }
	                        case "link": {
	                            switch(streamReader.getAttributeValue(null, "type")){
	                                case "boardgamemechanic" : {
	                                    Mechanic mechanic = new Mechanic();
	                                    mechanic.setId(streamReader.getAttributeValue(null, "id"));
	                                    mechanic.setName(streamReader.getAttributeValue(null, "value"));
	                                    newMechanics.add(mechanic);
	                                    break;
	                                }                                    
	                                case "boardgamecategory" : {
	                                    Category category = new Category();
	                                    category.setId(streamReader.getAttributeValue(null, "id"));
	                                    category.setName(streamReader.getAttributeValue(null, "value"));
	                                    newCategories.add(category);
	                                    break;
	                                }     
	                                case "boardgameexpansion" : {
	                                	// inbound = parent game
	                                	if(streamReader.getAttributeValue(null, "inbound") == null) {
		                                    Boardgame expansion = new Boardgame();
		                                    expansion.setId(streamReader.getAttributeValue(null, "id"));
		                                    expansion.setName(streamReader.getAttributeValue(null, "value").replaceAll("�", "-"));
		                                    newExpansions.add(expansion);
	                                	}
	                                    break;
	                                }     
	                            }
	                            //newBoardgame.setName(streamReader.getElementText());
	                            break;
	                        }
	                    }
	                }
	                if (streamReader.isEndElement()) {
	                    if (streamReader.getLocalName() == "item") {
	                        boardgames.put(newBoardgame.getId(), newBoardgame);
	                    }
	                }
	                streamReader.next();
	            }
	    		
	    	} while (indexList < ids.size());        
            
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (XMLStreamException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return boardgames;
    }      
}
