package com.ilmion.bggcompanion.services;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ilmion.bggcompanion.entities.Boardgame;
import com.ilmion.bggcompanion.entities.BoardgameStats;
import com.ilmion.bggcompanion.entities.Play;
import com.ilmion.bggcompanion.entities.Player;
import com.ilmion.bggcompanion.entities.PlayerStats;

public class BggStatistic {
    
    private Connection con;
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    
    private String location; 
    private Date dateDebut;
    private Date dateFin; 
    private List<String> listJeux;
    private boolean exclusion;
    
    public BggStatistic(Connection con) {
        this.con = con;
        
        location = null; 
        dateDebut = null;
        dateFin = null; 
        listJeux = null;
        exclusion = false;
        
    }
    
    public void setCondition(String location, Date dateDebut, Date dateFin, List<String> jeux, boolean exclusion) {
        this.location = location;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.listJeux = jeux;
        this.exclusion = exclusion;
    }
    
    public Integer getNbPlays(){

        Integer values = 0;
        ResultSet rs;
        try {     
            // TODO : NE pas mettre le prametre commme ça, très dangereux
            StringBuilder req = new StringBuilder();
            req.append("SELECT COUNT(*) plays FROM plays");
            if(listJeux != null && listJeux.size() > 0) req.append(" INNER JOIN games ON plays.bgg_id=games.bgg_id");
            req.append(createWhere(false));
            //System.out.println(req.toString());
            rs = con.createStatement().executeQuery(req.toString());
            rs.next();
            values = rs.getInt("plays");
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return values;
    }
    
    public Integer getNbDayPlayed(){

        Integer values = 0;
        ResultSet rs;
        try {     
            // TODO : NE pas mettre le prametre commme ça, très dangereux
            StringBuilder req = new StringBuilder();
            req.append("SELECT COUNT(DISTINCT plays.play_date) plays FROM plays");
            if(listJeux != null && listJeux.size() > 0) req.append(" INNER JOIN games ON plays.bgg_id=games.bgg_id");
            req.append(createWhere(false));
            //System.out.println(req.toString());
            rs = con.createStatement().executeQuery(req.toString());
            rs.next();
            values = rs.getInt("plays");
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return values;
    }

    public Map<DayOfWeek, Integer> getNbDayPlayedPerDayofWeek(){
        Map<DayOfWeek, Integer> map = new HashMap<DayOfWeek, Integer>();
        
        ResultSet rs;
        try {
         // TODO : NE pas mettre le prametre commme �a, tr�s dangereux
            StringBuilder req = new StringBuilder();
            req.append("SELECT DAYOFWEEK(plays.play_date) dday, COUNT(DISTINCT plays.play_date) nbDay FROM plays");
            if(listJeux != null && listJeux.size() > 0) req.append(" INNER JOIN games ON plays.bgg_id=games.bgg_id");
            req.append(createWhere(true));
            req.append(" GROUP BY DAYOFWEEK(play_date)");
            //System.out.println(req.toString());
            rs = con.createStatement().executeQuery(req.toString());

            while (rs.next()) {
                DayOfWeek day = DayOfWeek.SUNDAY;
                switch(rs.getInt("dday")) {
                    case 1: day=DayOfWeek.SUNDAY; break;
                    case 2: day=DayOfWeek.MONDAY; break;
                    case 3: day=DayOfWeek.TUESDAY; break;
                    case 4: day=DayOfWeek.WEDNESDAY; break;
                    case 5: day=DayOfWeek.THURSDAY; break;
                    case 6: day=DayOfWeek.FRIDAY; break;
                    case 7: day=DayOfWeek.SATURDAY; break;
                }
                map.put(day, rs.getInt("nbDay"));                
            }
            
            for(DayOfWeek day : DayOfWeek.values()){
                if(!map.containsKey(day)) {
                    map.put(day, 0);
                }
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return map;
    }
    
    public List<PlayerStats> getPlayerStats(){
        List<PlayerStats> list = new ArrayList<PlayerStats>();
        ResultSet rs;
        try {
         // TODO : NE pas mettre le prametre commme ça, très dangereux
            StringBuilder req = new StringBuilder();
            req.append("SELECT pp.name, COUNT(DISTINCT p.play_date) daysPlayed, COUNT(*) plays, COUNT(CASE WHEN p.incomplete=FALSE AND p.nowinstats=FALSE THEN 1 END) playsCompleted, COUNT(CASE WHEN pp.win AND p.nowinstats=FALSE THEN 1 END) win, COUNT(CASE WHEN pp.win AND p.incomplete=FALSE AND p.nowinstats=FALSE THEN 1 END) winCompleted FROM plays_players pp INNER JOIN plays p ON pp.play_id=p.play_id");
            if(listJeux != null && listJeux.size() > 0) req.append(" INNER JOIN games ON p.bgg_id=games.bgg_id");
            req.append(createWhere(true));
            req.append(" GROUP BY pp.name ORDER BY plays DESC, pp.name");
           // System.out.println(req.toString());
            rs = con.createStatement().executeQuery(req.toString());

            while (rs.next()) {
                Player player = new Player();
                player.setName(rs.getString("name"));
                
                PlayerStats playerStats = new PlayerStats();
                playerStats.setPlayer(player);
                playerStats.setNbDayPlayed(rs.getInt("daysPlayed"));
                playerStats.setNbPlay(rs.getInt("plays"));
                playerStats.setNbWin(rs.getInt("win"));
                playerStats.setNbWinCompleted(rs.getInt("winCompleted"));
                playerStats.setNbPlayCompleted(rs.getInt("playsCompleted"));
                
               //System.out.println(player.getName());
                
                list.add(playerStats);
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return list;
    }
    
    public List<BoardgameStats> getBoardgameStats(){
        List<BoardgameStats> list = new ArrayList<BoardgameStats>();
        
        ResultSet rs;
        try {
         // TODO : NE pas mettre le prametre commme �a, tr�s dangereux
            StringBuilder req = new StringBuilder();
            req.append("SELECT games.name, games.bgg_id, COUNT(*) plays, COUNT(CASE WHEN p.incomplete=FALSE THEN 1 END) playsCompleted FROM games INNER JOIN plays p ON p.bgg_id=games.bgg_id");
            req.append(createWhere(true));
            req.append(" GROUP BY games.name, games.bgg_id ORDER BY plays DESC, games.name");
            //System.out.println(req.toString());
            rs = con.createStatement().executeQuery(req.toString());

            while (rs.next()) {
                Boardgame boardgame = new Boardgame();
                boardgame.setName(rs.getString("name"));
                boardgame.setId(rs.getString("bgg_id"));
                
                BoardgameStats boardgameStats = new BoardgameStats();
                boardgameStats.setBoardgame(boardgame);
                boardgameStats.setNbPlay(rs.getInt("plays"));
                boardgameStats.setNbPlayCompleted(rs.getInt("playsCompleted"));
                
                list.add(boardgameStats);
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return list;
    }
    
    private String createWhere(boolean existingWhere) {
        StringBuilder req = new StringBuilder();
        if(location != null || dateDebut != null || dateFin != null || (listJeux != null && listJeux.size() > 0)) {
            boolean where = existingWhere;
            if(!where) req.append(" WHERE");
            if(location != null) {
                if(where) {
                    req.append(" AND");
                }
                req.append(" location='" + location + "'");
                where = true;
            }
            if(dateDebut != null){
                if(where) {
                    req.append(" AND");
                }
                req.append(" play_date>='" + formatter.format(dateDebut) + "'");
                where = true;
            }
            if(dateFin != null){
                if(where) {
                    req.append(" AND");
                }
                req.append(" play_date<='" + formatter.format(dateFin) + "'");
                where = true;
            }
            if(listJeux != null && listJeux.size() > 0) {
                if(where) {
                    req.append(" AND");
                }
                req.append(" games.bgg_id");
                if(exclusion){
                    req.append(" NOT");
                }
                req.append(" IN (");
                int i = 0;
                for(String jeu : listJeux) {
                    if(i>0) req.append(",");
                    req.append(jeu);
                    ++i;
                }
                req.append(")");
            }
        }
        return req.toString();
    }
    
    public List<Play> getPlaysWithoutName(String name) {
        List<Play> plays = new ArrayList<Play>();
        
        ResultSet rs;
        try {
            StringBuilder req = new StringBuilder();
            //req.append("SELECT pp.play_id FROM plays_players pp WHERE pp.name='" + name + "'");
            System.out.println(req.toString());
            req.append("SELECT p.play_date, p.location, pp.name FROM plays p JOIN plays_players pp ON pp.play_id = p.play_id AND pp.play_id NOT IN (SELECT pp.play_id FROM plays_players pp WHERE pp.name='" + name + "')");
            /*req.append("SELECT games.name, games.bgg_id, COUNT(*) plays, COUNT(CASE WHEN p.incomplete=FALSE THEN 1 END) playsCompleted FROM games INNER JOIN plays p ON p.bgg_id=games.bgg_id");
            req.append(createWhere(true));
            req.append(" GROUP BY games.name, games.bgg_id ORDER BY plays DESC, games.name");
            //System.out.println(req.toString());*/
            rs = con.createStatement().executeQuery(req.toString());
            
            while (rs.next()) {
                Play play = new Play();
                play.setDate(rs.getDate("play_date"));
                
                //play.setLocation(rs.getString("play_id"));
                //play.setLocation(rs.getString("name"));
                play.setLocation(rs.getString("location"));
                
                /*
                boardgame.setName(rs.getString("name"));
                boardgame.setId(rs.getString("bgg_id"));
                
                BoardgameStats boardgameStats = new BoardgameStats();
                boardgameStats.setBoardgame(boardgame);
                boardgameStats.setNbPlay(rs.getInt("plays"));
                boardgameStats.setNbPlayCompleted(rs.getInt("playsCompleted"));*/
                
                plays.add(play);
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return plays;        
    }
    
    public Map<String, Integer> getBggUserNbPlay(){
        Map<String, Integer> map = new HashMap<String, Integer>();
        
        ResultSet rs;
        try {
            StringBuilder req = new StringBuilder();
            req.append("SELECT bgguser, COUNT(*) nbplay FROM plays");
            if(listJeux != null && listJeux.size() > 0) req.append(" INNER JOIN games ON plays.bgg_id=games.bgg_id");
            req.append(createWhere(true));
            req.append(" GROUP BY bgguser");
            //System.out.println(req.toString());
            rs = con.createStatement().executeQuery(req.toString());

            while (rs.next()) {
                map.put(rs.getString("bgguser"), rs.getInt("nbplay"));                
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return map;
    }
}
