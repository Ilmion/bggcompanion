package com.ilmion.bggcompanion.services;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.commons.lang3.StringUtils;

import com.ilmion.bggcompanion.exceptions.BoardgamegeekException;
import com.ilmion.bggcompanion.http.URLFactory;

public class BggBDUpdater {
    
    private static BggBDUpdater instance = null;
    private boolean init = false;
    private boolean updated = false;
    
    private BggBDUpdater() { 
    }
    
    public static BggBDUpdater getInstance() 
    { 
        if (instance == null) 
            instance = new BggBDUpdater(); 
  
        return instance; 
    }
    
    private class PlayPlayerDTO {
        public String name;
        public Double score;
        public boolean win;
    }
    
    private Connection con;
    private String[] users;
    
    Set<Integer> boardgamesAdded = new HashSet<Integer>();
    
    private int page;
    private int nbPlays;
    private int totalPlays;
    private int nouvTotalPlays;
    
    private String masterUser = "Ilmion";     
    
    public synchronized void init(Connection con, String[] users) {
        if(!init){
            this.con = con;
            this.users = users;
            this.init = true;
        }
    }

    public synchronized void updateBD() throws BoardgamegeekException {
        if(!updated){
            try {
                con.createStatement().executeUpdate("DELETE FROM plays");
                con.createStatement().executeUpdate("DELETE FROM plays_players");
                con.createStatement().executeUpdate("DELETE FROM games");
                
                for(String user : users){
                    page = 1;
                    nbPlays = 0;
                    totalPlays = 0;
                    nouvTotalPlays = 0;
                    // On demande la premier page
                    TraiterPlayPageSuivante(user);           
                    // On garde le total et on boucle tant qu'il n'y a plus de play a aller chercher
                    while(nbPlays < totalPlays) {               
                        // On demande la page suivante
                        TraiterPlayPageSuivante(user);                 
                    }
                    con.commit();
                    updated = true;
                }
            }
            catch(SQLException e) {
             // TODO Auto-generated catch block
                e.printStackTrace();
                try {
                    con.rollback();
                } catch (SQLException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
                
            }
            catch(BoardgamegeekException e){
                try {
                    con.rollback();
                    throw e;
                } catch (SQLException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        
        }
    }

    private void TraiterPlayPageSuivante(String user) throws SQLException, BoardgamegeekException {
        
        String realURL = URLFactory.getPlaysUrl(user, page);
        //System.out.println(realURL);        
        String playsXML = BggRequester.getBggXML(realURL);
        
        ParseXML(user, playsXML);
        
        if(nouvTotalPlays != totalPlays) {
            // Il y a eu un changement, on change de page
            con.rollback();
            totalPlays = nouvTotalPlays;
            page = 1;
        }
        else {
            ++page;
        }          
    }

    private void ParseXML(String user, String playsXML) throws SQLException {
        try {
            int play_id = 0;
            int boardgame_id = 0;
            String play_date = null;
            String play_location = null;
            String play_incomplete = null;
            String play_nowinstats = null;
            String play_comments = null;
            List<PlayPlayerDTO> playPlayers = null;
            
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            InputStream in = new ByteArrayInputStream(playsXML.getBytes("utf-8"));
            XMLStreamReader streamReader = inputFactory.createXMLStreamReader(in);
            streamReader.nextTag(); // Advance to "plays" element
            while (streamReader.hasNext()) {
                if (streamReader.isStartElement()) {
                    switch (streamReader.getLocalName()) {
                        case "plays" : {
                            nouvTotalPlays = Integer.parseInt(streamReader.getAttributeValue(null, "total"));
                            if(totalPlays == 0) {
                                totalPlays = nouvTotalPlays;
                            }                            
                            break;
                        }
                        case "play": {
                            playPlayers = new ArrayList<PlayPlayerDTO>();
                            play_id = Integer.parseInt(streamReader.getAttributeValue(null, "id"));
                            play_date = streamReader.getAttributeValue(null, "date");
                            play_location = streamReader.getAttributeValue(null, "location");
                            play_incomplete = streamReader.getAttributeValue(null, "incomplete");
                            play_nowinstats = streamReader.getAttributeValue(null, "nowinstats");
                            ++nbPlays;
                            break;
                        }
                        case "item": {
                            String boardgame_name = streamReader.getAttributeValue(null, "name");
                            boardgame_id = Integer.parseInt(streamReader.getAttributeValue(null, "objectid"));

                            if(!boardgamesAdded.contains(boardgame_id)){
                                PreparedStatement sql = con.prepareStatement("INSERT INTO games (bgg_id, name) VALUES (?,?)");
                                sql.setInt(1, boardgame_id);
                                sql.setString(2, boardgame_name);
                                sql.executeUpdate();

                                boardgamesAdded.add(boardgame_id);
                            }
                            break;
                        }
                        case "comments" : {
                            play_comments = streamReader.getElementText();
                            break;
                        }
                        case "player": {
                            PlayPlayerDTO playPlayer = new PlayPlayerDTO();
                            playPlayer.name = streamReader.getAttributeValue(null, "name");
                            playPlayer.win = streamReader.getAttributeValue(null, "win").equals("1");
                            String sScore = streamReader.getAttributeValue(null, "score");
                            sScore = sScore.replace(",", ".");
                            if(StringUtils.isNotEmpty(sScore)) {
                                playPlayer.score = Double.parseDouble(sScore);
                            }
                            else {
                                playPlayer.score = null;                                
                            }
                            playPlayers.add(playPlayer);
                            break;
                        }
                    }
                }
                else if(streamReader.isEndElement()) {
                    if(streamReader.getLocalName().equals("play"))
                    {
                        
                        if(user.equals(masterUser) || (!user.equals(masterUser) && !playPlayers.stream().filter(p -> p.name.equals("Bruno Labelle")).findAny().isPresent())) {
                            // MasterUser
                            for(PlayPlayerDTO playPlayer : playPlayers) {
                                PreparedStatement sql = con.prepareStatement("INSERT INTO plays_players VALUES (?,?,?,?)");
                                sql.setInt(1, play_id);
                                sql.setString(2, playPlayer.name);
                                sql.setBoolean(3, playPlayer.win);
                                
                                if(playPlayer.score != null) {
                                    sql.setDouble(4, playPlayer.score);
                                }
                                else {
                                    sql.setNull(4, Types.NULL);                                
                                }
                                sql.executeUpdate();
                            }
                            
                            PreparedStatement sql = con.prepareStatement("INSERT INTO plays VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
                            sql.setInt(1, play_id);
                            sql.setInt(2, boardgame_id);
                            sql.setString(3, play_date);
                            sql.setString(4, play_location);
                            sql.setBoolean(5, play_incomplete.equals("1"));
                            sql.setBoolean(6, play_nowinstats.equals("1"));
                            sql.setString(7, play_comments);
                            sql.setString(8, user);
                            sql.executeUpdate();
                        }
                        boardgame_id = 0;
                        play_comments = null;
                    }                    
                }
                streamReader.next();
            }
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (XMLStreamException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
    }
}
