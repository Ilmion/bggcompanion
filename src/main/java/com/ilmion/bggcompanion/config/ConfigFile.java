package com.ilmion.bggcompanion.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigFile {

    private static final ConfigFile instance = new ConfigFile();
    
    
    private Properties prop = new Properties();
    
    //private constructor to avoid client applications to use constructor
    private ConfigFile(){
        
        InputStream input = null;
        
        try {        
            String filename = "app.properties";
            input = this.getClass().getClassLoader().getResourceAsStream(filename);
            if(input==null){
                    System.out.println("Sorry, unable to find " + filename);
                return;
            }

            //load a properties file from class path, inside static method
            prop.load(input);
 
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally{
            if(input!=null){
                try {
                input.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            }
        }
    }

    public static ConfigFile getInstance(){
        return instance;
    }

    public String getValue(String key){
        return prop.getProperty(key);
    }
}
