package com.ilmion.bggcompanion.helper;

public class CollectionPrinterFilter {
	private boolean excludeFanExpansion = false;
	private boolean excludeWishlistExpansion = false;
	private boolean excludePreorderedExpansion = false;
	private boolean trimExpansion = false;
	
	public boolean isExcludeFanExpansion() {
		return excludeFanExpansion;
	}
	public void setExcludeFanExpansion(boolean excludeFanExpansion) {
		this.excludeFanExpansion = excludeFanExpansion;
	}
	
	public boolean isTrimExpansion() {
		return trimExpansion;
	}
	public void setTrimExpansion(boolean trimExpansion) {
		this.trimExpansion = trimExpansion;
	}
	
    public boolean isExcludeWishlistExpansion() {
        return excludeWishlistExpansion;
    }
    public void setExcludeWishlistExpansion(boolean excludeWishlistExpansion) {
        this.excludeWishlistExpansion = excludeWishlistExpansion;
    }
    
    public boolean isExcludePreorderedExpansion() {
        return excludePreorderedExpansion;
    }
    public void setExcludePreorderedExpansion(boolean excludePreorderedExpansion) {
        this.excludePreorderedExpansion = excludePreorderedExpansion;
    }
}
