package com.ilmion.bggcompanion.helper;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.ilmion.bggcompanion.entities.Boardgame;
import com.ilmion.bggcompanion.services.BggCollection;

public class CollectionPrinter {
	private CollectionPrinterFilter filter;
	private Set<String> expansionPrinted = null; 
	boolean trimmedExpansion = false;
	
	public CollectionPrinterFilter getFilter() {
		return filter;
	}

	public void setFilter(CollectionPrinterFilter filter) {
		this.filter = filter;
	}

	public String printExpansion(BggCollection collectionWithoutExpansion, BggCollection collectionWithExpansion) {
	    expansionPrinted = new HashSet<String>();
		StringBuilder result = new StringBuilder();
		
		boolean missingExpansion = printBoardgameTree(0, collectionWithoutExpansion.getCollection(), collectionWithExpansion, result);
		
		if(!missingExpansion){
			result.append("Congratulation, you have all the expansions of all your game.");
		}
		
		return result.toString();
	}

	private boolean printBoardgameTree(int i, List<Boardgame> base, BggCollection full, StringBuilder result) {
		boolean recursivemissingExpansion = false;
		for(Boardgame boardgame : base){
		    if(filter == null || !filter.isExcludeFanExpansion() || (filter.isExcludeFanExpansion() && !boardgame.hasCategory("2687"))) {
    			boolean missingExpansion = false;
    			int offset = result.length();
    			trimmedExpansion = false;
    			if(boardgame.getExpansions().size() > 0) {
    				missingExpansion = printBoardgameTree(i+1, boardgame.getExpansions(), full, result);
    			}
    			else {
    			    Boardgame expansion = full.get(boardgame.getId());
    			    if(expansion != null) {
    			        //missingExpansion = !full.ownBoardgame(boardgame.getId());
    			         if(!expansion.isOwned()) {
    			            //missingExpansion = true;
    			            if(filter != null) {
    			                if(!((filter.isExcludeWishlistExpansion() && expansion.isWishlist()) || (filter.isExcludePreorderedExpansion() && expansion.isPreordered()))) {
    			                    missingExpansion = true;                       
    			                }
                            }
    			        }
    			    }
    			    else {
    			        missingExpansion = true;
    			    }
    			}
    			if(missingExpansion) {
    			    if(filter == null || !filter.isTrimExpansion() || (filter.isTrimExpansion() && !expansionPrinted.contains(boardgame.getId()))) {
        				StringBuilder currentGame = new StringBuilder();
        				currentGame.append("<div class=\"boargamename\">");
        				for(int j = 0; j < i; ++j) currentGame.append("----");
        				if(i > 0 && full.ownBoardgame(boardgame.getId())) currentGame.append("[own] ");
        				currentGame.append("<a href=\"https://boardgamegeek.com/boardgame/" + boardgame.getId() + "\">" + boardgame.getName() + "</a>");
        				if(trimmedExpansion) currentGame.append(" [shared expansion not listed]");
        				currentGame.append("</div>\n");
        				result.insert(offset, currentGame);
        				
        				if(i > 0) expansionPrinted.add(boardgame.getId());
    			    }
    			    else {
    			        trimmedExpansion = true;
    			    }
    			}
    			recursivemissingExpansion = recursivemissingExpansion || missingExpansion;
		    }
		}
		return recursivemissingExpansion;
	}
}
