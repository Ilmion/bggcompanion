package com.ilmion.bggcompanion.http;

import com.ilmion.bggcompanion.config.ConfigFile;
import com.ilmion.bggcompanion.filters.CollectionFilter;
import com.ilmion.bggcompanion.filters.FilterIncludeExclude;

public class URLFactory {
    
    private static boolean debug = Boolean.parseBoolean(ConfigFile.getInstance().getValue("debug"));
    
    private static String mechanicUrl = "https://boardgamegeek.com/browse/boardgamemechanic";
    private static String categoryUrl = "https://boardgamegeek.com/browse/boardgamecategory";
    private static String collectionUrl = "https://www.boardgamegeek.com/xmlapi2/collection";  
    private static String thingUrl = "https://www.boardgamegeek.com/xmlapi2/thing";
    private static String playsUrl = "https://www.boardgamegeek.com/xmlapi2/plays";
    
    public static String getMechanicUrl() {
        String url;
        if(debug) {            
            url = "http://localhost:8080/bggcompanion/mock/mechanicmock.htm";
        }
        else {           
            url = mechanicUrl;            
        }
        return url; 
    }
    
    public static String getCategoryUrl() {
        String url;
        if(debug) {            
            url = "http://localhost:8080/bggcompanion/mock/categorymock.htm";
        }
        else {           
            url = categoryUrl;            
        }
        return url; 
    }
    
    public static String getCollectionUrl(String user, CollectionFilter collectionFilters) {
        String url;
        
        StringBuilder filter =  new StringBuilder("");
        if(collectionFilters != null) {
            if(collectionFilters.getOwn() == FilterIncludeExclude.INCLUDE) {
                filter.append("&own=1");
            } 
            else if(collectionFilters.getOwn() == FilterIncludeExclude.EXCLUDE) {
                filter.append("&own=0");
            }
            
            if(collectionFilters.getPreordered() == FilterIncludeExclude.INCLUDE) {
                filter.append("&preordered=1");
            } 
            else if(collectionFilters.getPreordered() == FilterIncludeExclude.EXCLUDE) {
                filter.append("&preordered=0");
            }
            
            if(collectionFilters.getWishlist() == FilterIncludeExclude.INCLUDE) {
                filter.append("&wishlist=1");
            } 
            else if(collectionFilters.getWishlist() == FilterIncludeExclude.EXCLUDE) {
                filter.append("&wishlist=0");
            }
            
            if(collectionFilters.getExcludeExpansion()) {
                filter.append("&excludesubtype=boardgameexpansion");
            }
        } 
        
        if(debug) {            
            url = "http://localhost:8080/bggcompanion/mock/collection" + (collectionFilters != null && collectionFilters.getExcludeExpansion()?"Base":"Exp") + user + ".xml";
        }
        else {           
            url = collectionUrl + "?username="+ user + filter.toString();          
        }
        return url; 
    }
    
    public static String getThingUrl(String ids) {
        String url;
        if(debug) {            
            url = "http://localhost:8080/bggcompanion/mock/corkysruCollectionThings.xml";
        }
        else {           
            url = thingUrl + "?id=" + ids;            
        }
        return url; 
    }
    
    public static String getPlaysUrl(String user, int page) {
        String url;
        if(debug) {            
            url = "http://localhost:8080/bggcompanion/mock/plays" + user + page + ".xml";
        }
        else {           
            url = playsUrl + "?username="+ user + "&page=" + page;         
        }
        return url; 
    }
}
