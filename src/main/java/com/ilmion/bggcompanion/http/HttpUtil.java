package com.ilmion.bggcompanion.http;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import javax.security.cert.CertificateException;
import javax.security.cert.X509Certificate;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.conn.ssl.AllowAllHostnameVerifier;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


public class HttpUtil {
        
	private static int nbRetry = 3;
    // throws something
    private static HttpClient createHttpClient_AcceptsUntrustedCerts() {

        CloseableHttpClient httpClient = null;
        try {
            httpClient = HttpClients.custom().
                    setHostnameVerifier(new AllowAllHostnameVerifier()).
                    setSslcontext(new SSLContextBuilder().loadTrustMaterial(null, new TrustStrategy()
                    {
                        @Override
                        public boolean isTrusted(java.security.cert.X509Certificate[] arg0, String arg1) throws java.security.cert.CertificateException {
                            return true;
                        }
                    }).build()).build();
        } catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return httpClient;
        /*
        HttpClient client = null;    
             
        try {
            HttpClientBuilder b = HttpClientBuilder.create();
            SSLContext sslContext;
            
            sslContext = new SSLContextBuilder().loadTrustMaterial(null, new TrustAllStrategy()).build();
            b.setSSLContext(sslContext);
            
            SSLConnectionSocketFactory sslSocketFactory = new SSLConnectionSocketFactory(sslContext, new NoopHostnameVerifier());
            Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                    .register("http", PlainConnectionSocketFactory.getSocketFactory())
                    .register("https", sslSocketFactory)
                    .build();
         
            PoolingHttpClientConnectionManager connMgr = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
            b.setConnectionManager(connMgr);
         
            client = b.build();
            
        } catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } 
        
        return client;*/
    }

    // throws something
    public static String getHttpContent(String url) {
        String content = null;
        HttpClient httpClient = HttpUtil.createHttpClient_AcceptsUntrustedCerts();
        
        HttpGet httpGet = new HttpGet(url);
        HttpResponse httpResponse = null;
                
        try {
            int retry = 0;
            
            do {
                if(retry > 0 && httpResponse.getStatusLine().getStatusCode() == HttpURLConnection.HTTP_ACCEPTED){
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        // We ignore that
                    }
                }
                ++retry;
                httpResponse = httpClient.execute(httpGet);
                
            } while (httpResponse.getStatusLine().getStatusCode() == HttpURLConnection.HTTP_ACCEPTED && retry <= nbRetry);
            
            if(httpResponse.getStatusLine().getStatusCode() == HttpURLConnection.HTTP_OK){
                HttpEntity entity = httpResponse.getEntity();
                content = EntityUtils.toString(entity,"UTF-8");
                EntityUtils.consume(entity);
            }
            //else if(httpResponse.getStatusLine().getStatusCode() == HttpURLConnection.HTTP_ACCEPTED){
           //     // RetryException
           // 	System.out.println("RetryException, url : " + url);
           // }
            else {
                // AutreException
            	System.out.println("OtherException, url : " + url);
            }
            
        } catch (IOException e) {
            // TODO Auto-generated catch block
        	System.out.println(url);
            e.printStackTrace();
        } finally {
            HttpClientUtils.closeQuietly(httpClient);
            HttpClientUtils.closeQuietly(httpResponse);
        }
        
        return content;
    }
}
