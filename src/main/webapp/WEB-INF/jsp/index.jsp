<%@page import="com.ilmion.bggcompanion.exceptions.*, com.ilmion.bggcompanion.filters.*, com.ilmion.bggcompanion.helper.*"%>
<%@page import="com.ilmion.bggcompanion.services.*, com.ilmion.bggcompanion.entities.*, java.util.*, java.util.concurrent.*" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>BGG Ilmion's Companion</title>
</head>
<body>
<%
if(request.getParameter("username") == null) {
%>
This companion can sorts your boardgamegeek collection by game mechanics and category.
</br>
It can also list the expansion you do not have for boardgames you own.
It takes only the owned boardgame, exclude the expansion and ignore multiple copies of the same game.
</br>
It may take a while for boardgamegeek to generate the collection and <b>you may have to try again</b>.
</br> 
</br>
Enter you the Boardgamegeek username and press submit.</br>
  <form action="index" method="get">
  username:<input type="text" name="username" /input></br>
  <input type="checkbox" name="mechanic" value="1" checked>Mechanic</br>
  <input type="checkbox" name="category" value="1">Category</br>
  <fieldset>
  <input type="checkbox" name="missingexpansion" value="1">Missing Expansion</br>
  --<input type="checkbox" name="excludefanexp" value="1">Exclude Fan Expansion</br>
  --<input type="checkbox" name="excludePreordered" value="1">Exclude Expansion Preordred</br>
  --<input type="checkbox" name="excludeWishlist" value="1">Exclude Expansion on Wishlist</br>
  --<input type="checkbox" name="trimexp" value="1">Show same expansion of different game only in one game</br>
  </fieldset>
  <input type="submit" value="Submit">
  </form>
</br>
Created by <a href="https://boardgamegeek.com/user/Ilmion">Ilmion</a>.<br/> 
Hosted by Guillaume Benny (see is project <a href="https://guillaumebenny.com/">here</a>)
<%
}
else{
    long lStartTime = System.nanoTime();
    long lCollectionTime = System.nanoTime();
    long lInitTime = System.nanoTime();
	try
	{   
	    String checkMechanic = request.getParameter("mechanic");
	    String checkCategory = request.getParameter("category");
	    String checkMissingExpansion = request.getParameter("missingexpansion");
	    String checkExcludefanexp = request.getParameter("excludefanexp");
	    String checkExcludePreordered = request.getParameter("excludePreordered");
	    String checkExcludeWishlist = request.getParameter("excludeWishlist");
	    String checkTrimexp = request.getParameter("trimexp");
	    
	    CollectionFilter filterWithOutExp = new CollectionFilter();
	    CollectionFilter filterWithExp = new CollectionFilter();
	    filterWithOutExp.setOwn(FilterIncludeExclude.INCLUDE);
	    filterWithOutExp.setExcludeExpansion(true);
	    filterWithOutExp.setAllowDoublon(false);
	    //filterWithExp.setOwn(FilterIncludeExclude.INCLUDE);
	    filterWithExp.setAllowDoublon(false);
	        
	    BggCollection collectionWithOutExpansion = BggRequester.getCollection(request.getParameter("username"), filterWithOutExp);
	    BggCollection collectionWithExpansion = null;
	    if(checkMissingExpansion != null && checkMissingExpansion.compareTo("1") == 0) {
	    	collectionWithExpansion = BggRequester.getCollection(request.getParameter("username"), filterWithExp);
	    }
	    
	    lCollectionTime = System.nanoTime(); 
	    
	    collectionWithOutExpansion.loadDetail((checkMissingExpansion != null && checkMissingExpansion.compareTo("1") == 0));
	    
	    lInitTime = System.nanoTime();
%>
Results based on <%=request.getParameter("username") %>'s collection</br>
<%
	    
	    if(checkMechanic != null && checkMechanic.compareTo("1") == 0){
			List<Mechanic> mechanics = BggRequester.getMechanicsList();
			
			if(mechanics != null && mechanics.size() > 0){
%>
			  <table border=1>
			    <col align="left">
			    <col align="left">
			    <col align="center">
			    <tr>
			      <td><h2>Mechanic</h2></td>
			      <td><h2>Boardgames</h2></td>
			      <td><h2>% of collection</h2></td>
			    </tr>
<%
				for(Mechanic mechanic : mechanics){
				  float nbGame = 0;
%>
			    <tr>
			      <td valign="top"><div class="boargamename"><a href="https://boardgamegeek.com/boardgamemechanic/<%=mechanic.getId() %>" ><%=mechanic.getName() %></a></div></td>
<%
					for(Boardgame boardgame : collectionWithOutExpansion.getCollection()){
					    if(boardgame.hasMechanics(mechanic.getId())){
					        if(nbGame==0) {
%>            <td><%
					        }
					        %><div class="boargamename"><a href="https://boardgamegeek.com/boardgame/<%=boardgame.getId() %>" ><%=boardgame.getName() %></a></div><%
					        ++nbGame;
					    }
					}
					if(nbGame==0){
%>            <td bgcolor="#AA4B39"><%				    
					}
%>
			      </td>
			      <td  valign="top">
<%		        
					out.println(String.format("%.1f%%", nbGame/collectionWithOutExpansion.getCollection().size()*100));
%>        
			      </td>
			    </tr>
<%
				}
%>	
			  </table>
<%
			}
			else{
			    out.println("Error, could not get Mechanics list.");
			}
	    }
	    if(checkCategory != null && checkCategory.compareTo("1") == 0){
	        if(checkMechanic != null && checkMechanic == "1"){
	            out.println("</br></br>");
	        }
			List<Category> categories = BggRequester.getCategoryList();
			
			if(categories != null && categories.size() > 0){
%>
			  <table border=1>
			    <col align="left">
			    <col align="left">
			    <col align="center">
			    <tr>
			      <td><h2>Category</h2></td>
			      <td><h2>Boardgames</h2></td>
			      <td><h2>% of collection</h2></td>
			    </tr>
<%
				for(Category category : categories){
				  float nbGame = 0;
%>
			    <tr>
			      <td valign="top"><div class="boargamename"><a href="https://boardgamegeek.com/boardgamecategory/<%=category.getId() %>" ><%=category.getName() %></a></div></td>
<%
					for(Boardgame boardgame : collectionWithOutExpansion.getCollection()){
					    if(boardgame.hasCategory(category.getId())){
					        if(nbGame==0) {
%>            <td><%
					        }
					        %><div class="boargamename"><a href="https://boardgamegeek.com/boardgame/<%=boardgame.getId() %>" ><%=boardgame.getName() %></a></div><%
					        ++nbGame;
					    }
					}
					if(nbGame==0){
%>            <td bgcolor="#AA4B39"><%				    
					}
%>
			      </td>
			      <td valign="top">
<%		        
					out.println(String.format("%.1f%%", nbGame/collectionWithOutExpansion.getCollection().size()*100));
%>        
			      </td>
			    </tr>
<%
				}
%>	
			  </table>
<%
			}
			else{
			    out.println("Error, could not get Category list.");
			}
	    }
	    if(checkMissingExpansion != null && checkMissingExpansion.compareTo("1") == 0) {
	    	if((checkMechanic != null && checkMechanic == "1") || (checkCategory != null && checkCategory == "1")){
	            out.println("</br></br>");
	        }
%>
<h2>Missing Expansion</h2>
<%
			CollectionPrinter collectionPrinter = new CollectionPrinter();
			CollectionPrinterFilter collectionPrinterFilter = new CollectionPrinterFilter();
			collectionPrinterFilter.setExcludeFanExpansion(checkExcludefanexp != null && checkExcludefanexp.compareTo("1") == 0);
			collectionPrinterFilter.setExcludeWishlistExpansion(checkExcludeWishlist != null && checkExcludeWishlist.compareTo("1") == 0);
			collectionPrinterFilter.setExcludePreorderedExpansion(checkExcludePreordered != null && checkExcludePreordered.compareTo("1") == 0);
			collectionPrinterFilter.setTrimExpansion(checkTrimexp != null && checkTrimexp.compareTo("1") == 0);
			collectionPrinter.setFilter(collectionPrinterFilter);
			out.println(collectionPrinter.printExpansion(collectionWithOutExpansion,collectionWithExpansion));
	    }
	}
	catch(BoardgamegeekException e){
%>
Message from boardgamegeek : <%=e.getMessage() %>
<% 
	}
	long lEndTime = System.nanoTime();
	%>
	<br/><br/><br/>
	<div hidden>
	Start to Finish : <%=((lEndTime - lStartTime) / 1000000) %><br/>
	having collection : <%=((lCollectionTime - lStartTime) / 1000000) %><br/>
	boardgames initialized : <%=((lInitTime - lCollectionTime) / 1000000) %><br/>
	looping creating table : <%=((lEndTime - lInitTime) / 1000000) %><br/>
	</div>
	<%
}
%>
<br/><br/><br/>Version 0.1.0
</body>
</html>