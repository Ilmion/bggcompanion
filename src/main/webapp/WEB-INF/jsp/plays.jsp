<%@page import="com.ilmion.bggcompanion.exceptions.*, com.ilmion.bggcompanion.filters.*, com.ilmion.bggcompanion.helper.*"%>
<%@page import="com.ilmion.bggcompanion.services.*, com.ilmion.bggcompanion.entities.*, java.util.*, java.util.concurrent.*, java.sql.Connection, java.text.DateFormat, java.text.SimpleDateFormat, java.text.ParseException, java.time.DayOfWeek" %>
<%
String startParam = request.getParameter("start");
String endParam = request.getParameter("end");

DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
Date dateDebut = null;
Date dateFin = null;
try {
    if(startParam != null){
		dateDebut = formatter.parse(startParam);
    }
    if(endParam != null){
		dateFin = formatter.parse(endParam);
    }
}
catch(ParseException e){}

List<String> jeuxExclus = new ArrayList<String>();
jeuxExclus.add("200551"); //30 Rails
jeuxExclus.add("199242"); //Mini Rogue

String exclusParam = request.getParameter("exclus");
if(exclusParam != null && !exclusParam.isEmpty()){
	String[] exclusParamSplit = exclusParam.split(",");
	for(String exclu : exclusParamSplit){
	    jeuxExclus.add(exclu);
	}
}

String refresh = request.getParameter("refresh");
if(refresh != null && !refresh.isEmpty()){
	if(Boolean.parseBoolean(refresh)){
	    getServletContext().setAttribute("bdUpToDate", null);
	}
}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="css/style.css" />
<title>BGG Ilmion's Companion</title>
</head>
<body>

<div id="formChoix">
<form action="plays" method="get" id="formu">
Date d&eacute;but <input type="date" name="start" id="start" value="<%=(dateDebut!=null?formatter.format(dateDebut):"") %>">
Date Fin <input type="date" name="end" id="end" value="<%=(dateFin!=null?formatter.format(dateFin):"") %>">
Jeux Exclus <input type="text" name="exclus" id="exclus" value="<%=(request.getParameter("exclus")!=null?request.getParameter("exclus"):"")%>">
<input type="submit" value="Filtrer">
<input type="button" value="Reset" onclick="document.getElementById('start').value = '';document.getElementById('end').value = '';document.getElementById('exclus').value = ''; document.getElementById('formu').submit();">
</form>
</div>
<%
	try
	{  
    	Connection con = (Connection)getServletContext().getAttribute("con");
    	Object dbUptoDate = getServletContext().getAttribute("bdUpToDate");
    	if(dbUptoDate == null){
	        BggBDUpdater.getInstance().init(con, new String[]{"Ilmion","pguilbau2023"});
	        BggBDUpdater.getInstance().updateBD();
	        getServletContext().setAttribute("bdUpToDate", true);
	        %><span hidden>updated</span><%
    	}
		
	    BggStatistic bggStat = new BggStatistic(con);
	    
	    bggStat.setCondition("LQ", dateDebut, dateFin, jeuxExclus, true);
	    
	    Integer nbPlays = bggStat.getNbPlays();
	    Integer nbDays = bggStat.getNbDayPlayed();
%><div id="tableau1" class="tableauStat">
<div class="tableJoueurEtJour">
	<div class="tableJoueurs">
<%
	out.println("Journ&eacute;e : " + nbDays + " Partie : " + nbPlays + "</br>");
	List<PlayerStats> listPlayerStats = bggStat.getPlayerStats();
%>
	<table>
		<tr>
	       <th>Joueur</th>
	       <th>Journ&eacute;es</th>
			<th>Parties</th>
			<th>Parties Complet&eacute;es</th>
			<th>Victoire</th>
			<th>% Victoire</th>        	        
		</tr>
<%
	    for(PlayerStats playerStats : listPlayerStats) {
	        String pourcent = String.format("%.2f",(double)playerStats.getNbWinCompleted()/(double)playerStats.getNbPlayCompleted()*100);
%>
		<tr>
	        <td><%=playerStats.getPlayer().getName()%></td>
	        <td><%=playerStats.getNbDayPlayed()%></td>
			<td><%=playerStats.getNbPlay()%></td>
			<td><%=playerStats.getNbPlayCompleted()%></td>
			<td><%=playerStats.getNbWinCompleted()%></td>
			<td><%=pourcent + "%"%></td>	        	        
	   	</tr><%
	    }
	%></table>
	</div>
	<div class="joursJoues">
	<%
		Map<DayOfWeek, Integer> mapDayOfWeek = bggStat.getNbDayPlayedPerDayofWeek();
	%>
		<table>
		<tr><th>Jour</th><th>Jou&eacute;</th></tr>
		<tr><td>Lundi</td><td><%=mapDayOfWeek.get(DayOfWeek.MONDAY)%></td></tr>
		<tr><td>Mardi</td><td><%=mapDayOfWeek.get(DayOfWeek.TUESDAY)%></td></tr>
		<tr><td>Mercredi</td><td><%=mapDayOfWeek.get(DayOfWeek.WEDNESDAY)%></td></tr>
		<tr><td>Jeudi</td><td><%=mapDayOfWeek.get(DayOfWeek.THURSDAY)%></td></tr>
		<tr><td>Vendredi</td><td><%=mapDayOfWeek.get(DayOfWeek.FRIDAY)%></td></tr>
		</table>
	</div>
	<div class="playUser">
	<%
		Map<String, Integer> bggUserPlays = bggStat.getBggUserNbPlay();
	%>
		<table>
		<tr><th>User</th><th>Parties</th></tr>
		<%for(Map.Entry<String,Integer> bggUserPlay : bggUserPlays.entrySet()) { %>
		<tr><td><%=bggUserPlay.getKey()%></td><td><%=bggUserPlay.getValue()%></td></tr>
		<%}%>
		</table>
	</div>
</div>
<div class="jeuxJoues">
<%
List<BoardgameStats> listBoardgameStats = bggStat.getBoardgameStats();
out.println("Nombre de jeux diff&eacute;rent : " + listBoardgameStats.size() + "</br>");
%>
<table>
<tr>
<th>Jeu</th>
<th>Parties</th>
<th>Compl&eacute;t&eacute;es</th>
<th></th>
</tr><%
	    for(BoardgameStats boardgameStats : listBoardgameStats) {
	        %><tr>
	        <td><%=boardgameStats.getBoardgame().getName()%></td>
			<td><%=boardgameStats.getNbPlay()%></td>
			<td><%=boardgameStats.getNbPlayCompleted()%></td>
			<td><img alt="remove from result" src="img/minus.png" height="15" width="auto" onMouseOver="this.src='img/minus2.png'" onMouseOut="this.src='img/minus.png'" onclick="document.getElementById('exclus').value += '<%=boardgameStats.getBoardgame().getId()%>,'; document.getElementById('formu').submit();"></td>        	        
	    	</tr><%
	    }
%></table>
</div>
</div>
<%
	}
	catch(BoardgamegeekException e){
%>
Message from boardgamegeek : <%=e.getMessage()%>
<% 
	}
%>
</br>
Created by <a href="https://boardgamegeek.com/user/Ilmion">Ilmion</a>.<br/> 
Hosted by Guillaume Benny (see is project <a href="https://guillaumebenny.com/">here</a>)
</body>
</html>